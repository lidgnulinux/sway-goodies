# sway-goodies

## What's this ?

Sway-goodies is some (maybe) usable scripts for SwayWM. It consists of some scripts :

- **get_cursor_coordinate_wayland**

    This script will tell you about cursor coordinates.

- **scratchpad_window_list**

    Show list of scratchpad windows.

- **sway_toggle_desktop**

    Emulate toggle desktop feature (show and hide all windows).

- **sway_window_menu**

    Show a simple window menu (minimize, close, toggle floating, etc).

- **swaybar_toggle_position**

    Toggle swaybar's position (top / bottom).
